defmodule LiveHeroIcons.MixProject do
  use Mix.Project

  def project do
    [
      app: :live_hero_icons,
      version: "0.4.1-rc",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      description: "This wraps Tailwind's Hero Icons as Phoenix LiveView components",
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:castore, "~> 0.1"},
      {:ex_doc, "~> 0.0", only: [:dev, :test]},
      {:mint, "~> 1.2"},
      {:phoenix_live_view, "~> 0.15"},
      {:tesla, "~> 1.4"}
    ]
  end

  defp package do
    [
      name: "live_hero_icons",
      files: ~w(lib mix.exs README* LICENSE*),
      licenses: ["MIT"],
      links: %{"Gitlab" => "https://gitlab.com/normanganderson/live_hero_icons.git"}
    ]
  end
end
