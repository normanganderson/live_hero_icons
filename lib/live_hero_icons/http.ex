defmodule LiveHeroIcons.Http do
  use Tesla
  adapter(Tesla.Adapter.Mint)

  plug(Tesla.Middleware.FollowRedirects)

  @icon_version "0.4.1"

  def get do
    temp_dir = System.tmp_dir!()
    icon_zip_path = Path.join([temp_dir, "v#{@icon_version}.zip"])

    get("https://github.com/tailwindlabs/heroicons/archive/v#{@icon_version}.zip")
    |> case do
      {:ok, %Tesla.Env{body: body, status: 200}} ->
        File.write!(icon_zip_path, body)

        unzipped_path = Path.join([temp_dir, "heroicons-#{@icon_version}"])
        {_, 0} = System.cmd("unzip", ["-o", icon_zip_path, "-d", temp_dir])
        src_path = Path.join([unzipped_path, "src"])

        icon_types =
          File.ls!(src_path)
          |> Enum.filter(&(&1 in ["solid", "outline"]))

        icon_types
        |> Enum.reduce(%{}, fn type, acc ->
          type_path = Path.join([src_path, type])
          Map.put(acc, type, File.ls!(type_path))
        end)
        |> Enum.map(fn {type, file_list} ->
          {
            type,
            file_list
            |> Enum.filter(&(Path.extname(&1) == ".svg"))
            |> Enum.map(fn file ->
              Path.join([src_path, type, file])
            end)
          }
        end)
    end
  end
end
